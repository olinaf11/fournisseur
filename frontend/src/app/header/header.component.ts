import { Component, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  heading = 1
  constructor(private renderer: Renderer2, private el: ElementRef){}
  ngOnInit(): void {
    this.Nav()
  }

  change_heading(heading : number){
    this.heading = heading
    console.log(heading)
  }
  Nav():void {
    const sidenav = this.el.nativeElement.querySelector('#mySidenav');
    const width = sidenav.style.width;

    if (width === '0px' || width === '') {
      this.renderer.setStyle(sidenav, 'width', '250px');
      this.toggleClass('.animated-icon', 'open');
    } else {
      this.renderer.setStyle(sidenav, 'width', '0px');
      this.toggleClass('.animated-icon', 'open');
    }
  }

  private toggleClass(selector: string, className: string): void {
    const elements = this.el.nativeElement.querySelectorAll(selector);
    elements.forEach((element: HTMLElement) => {
      element.classList.toggle(className);
    });
  }

}
