package com.example.syscom.controllers;

import com.example.syscom.models.Article;
import com.example.syscom.repository.ArticleRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class ArticleController {
    private final ArticleRepository articleRepository;

    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping("/article/get")
    public Iterable<Article> allArticles(){
        return this.articleRepository.findAll();
    }

    @PostMapping("/article/post")
    public Article save(@RequestBody Article article){
        return this.articleRepository.save(article);
    }

    @DeleteMapping("/article/delete/{id}")
    public void delete(@PathVariable("id") int idArticle) {
        this.articleRepository.deleteById((long) idArticle);
    }

    @PutMapping("/article/update")
    public Article updateArticle(@RequestBody Article article) {
        return this.articleRepository.updateArticleBy(article.getId(), article.getNom());
    }
}
