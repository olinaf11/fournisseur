package com.example.syscom.controllers;

import com.example.syscom.models.Clients;
import com.example.syscom.repository.ClientsRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientsController {
    private final ClientsRepository clientsRepository;

    public ClientsController(ClientsRepository clientsRepository) {
        this.clientsRepository = clientsRepository;
    }

    @GetMapping("/clients/all")
    public Iterable<Clients> getAllClients(){
        return this.clientsRepository.findAll();
    }

    @PostMapping("/clients/save")
    public Clients save(@RequestBody Clients client){
        return this.clientsRepository.save(client);
    }

    @DeleteMapping("/clients/delete/{id}")
    public void delete(@PathVariable("id") int id){
        this.clientsRepository.deleteById((long) id);
    }

    @PutMapping("/clients/update")
    public Clients update(@RequestBody Clients clients){
        return this.clientsRepository.updateClientsBy(clients.getId(), clients.getNom());
    }
}
