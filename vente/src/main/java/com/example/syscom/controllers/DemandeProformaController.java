package com.example.syscom.controllers;

import com.example.syscom.models.Clients;
import com.example.syscom.models.DemandeProforma;
import com.example.syscom.repository.DemandeProformaRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class DemandeProformaController {
    private final DemandeProformaRepository demandeProformaRepository;

    public DemandeProformaController(DemandeProformaRepository demandeProformaRepository) {
        this.demandeProformaRepository = demandeProformaRepository;
    }

    @GetMapping("/demandeProforma/all")
    public Iterable<DemandeProforma> getAllDemande(){
        return this.demandeProformaRepository.findAll();
    }

    @PostMapping("/demadeProforma/save")
    public DemandeProforma save(@RequestBody DemandeProforma demandeProforma){
        return this.demandeProformaRepository.save(demandeProforma);
    }

    @DeleteMapping("/demandeProforma/delete/{id}")
    public void delete(@PathVariable("id") int id){
        this.demandeProformaRepository.deleteById((long) id);
    }
}
