package com.example.syscom.controllers;

import com.example.syscom.models.EntreeStock;
import com.example.syscom.repository.EntreeStockRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class EntreeStockController {
    private final EntreeStockRepository entreeStockRepository;

    public EntreeStockController(EntreeStockRepository entreeStockRepository) {
        this.entreeStockRepository = entreeStockRepository;
    }

    @GetMapping("/entreeStock/all")
    public Iterable<EntreeStock> getAllEntreeStock(){
        return this.entreeStockRepository.findAll();
    }

    @PostMapping("/entreeStock/save")
    public EntreeStock save(@RequestBody EntreeStock entreeStock){
        return this.entreeStockRepository.save(entreeStock);
    }

    @DeleteMapping("/entreeStock/delete/{id}")
    public void delete(@PathVariable("id") int id){
        this.entreeStockRepository.deleteById((long) id);
    }
}
