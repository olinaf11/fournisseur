package com.example.syscom.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class EntreeStock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    int article;
    double quantite;
    int unite;

    public EntreeStock(int id, int article, double quantite, int unite) {
        this.id = id;
        this.article = article;
        this.quantite = quantite;
        this.unite = unite;
    }

    public EntreeStock() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public int getUnite() {
        return unite;
    }

    public void setUnite(int unite) {
        this.unite = unite;
    }
}
