package com.example.syscom.repository;

import com.example.syscom.models.Article;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Long> {
    @Query("UPDATE Article SET nom = ?2 WHERE id = ?1")
    @Modifying
    public Article updateArticleBy(int idArticle, String nom);
}
