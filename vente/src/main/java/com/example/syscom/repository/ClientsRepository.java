package com.example.syscom.repository;

import com.example.syscom.models.Article;
import com.example.syscom.models.Clients;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ClientsRepository extends CrudRepository<Clients, Long> {
    @Query("UPDATE Clients SET nom = ?2 WHERE id = ?1")
    @Modifying
    public Clients updateClientsBy(int id, String nom);
}
