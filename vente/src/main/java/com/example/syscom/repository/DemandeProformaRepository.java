package com.example.syscom.repository;

import com.example.syscom.models.DemandeProforma;
import org.springframework.data.repository.CrudRepository;

public interface DemandeProformaRepository extends CrudRepository<DemandeProforma, Long> {
}
