package com.example.syscom.repository;

import com.example.syscom.models.EntreeStock;
import org.springframework.data.repository.CrudRepository;

public interface EntreeStockRepository extends CrudRepository<EntreeStock, Long> {
}
